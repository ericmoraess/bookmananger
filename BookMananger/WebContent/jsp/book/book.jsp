<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Mananger Books</title>
</head>
<body>
	<div id="busca">
		<table>
			<tr>
				<td>Title:</td>
				<td><input type="text" value=""></td>
			</tr>
			<tr>
				<td>Category:</td>
				<td><select id="category">
						<option value="1">Programming</option>
						<option value="2">Web Programming</option>
						<option value="3">Test</option>
				</select></td>
			</tr>
			<tr>
				<td>Publish Year:</td>
				<td><select id="year">
						<option value="1">2020</option>
						<option value="2">2019</option>
						<option value="3">2018</option>
				</select>
			</tr>
		</table>
	</div>

	<div id="resultado">
		<table border="1">
			<tr>
				<th>Title</th>
				<th>Category</th>
				<th>Year Publish</th>
				<th>Change</th>
				<th>Delete</th>
			</tr>
			<tr>
				<td>Clean Code</td>
				<td>Programming</td>
				<td>2012</td>
				<td><input type="checkbox"></td>
				<td><input type="checkbox"></td>
			</tr>
		</table>
	</div>
</body>
</html>
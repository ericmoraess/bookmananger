package br.com.bookmananger.model;

public class Book {
	private Integer Id;
	private String name;
	private String category;
	private String yearPublish;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getYearPublish() {
		return yearPublish;
	}

	public void setYearPublish(String yearPublish) {
		this.yearPublish = yearPublish;
	}
}

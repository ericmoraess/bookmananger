package br.com.bookmananger.constants;

public abstract class Constants {
	public static final String BASE_PATH = "/jsp";
	public static final String EXTENSION_JSP = ".jsp";
	public static final String PATH_SEPARATOR = "/";
}

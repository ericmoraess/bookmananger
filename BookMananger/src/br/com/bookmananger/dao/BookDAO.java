package br.com.bookmananger.dao;

import java.util.List;

import br.com.bookmananger.model.Book;

public interface BookDAO {
	List<Book> getAllBook();
	Book getBookByName(int isbn);
	void saveBook(Book book);
	void deleteBook(Book book);
}

package br.com.bookmananger.dao;

import static br.com.bookmananger.dao.config.DatabaseConnection.initializeDatabase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.bookmananger.model.Book;

public class BookDaoImpl implements BookDAO {
	private Connection conn;

	public BookDaoImpl() throws ClassNotFoundException, SQLException {
		conn = initializeDatabase();
	}

	@Override
	public List<Book> getAllBook() {
		List<Book> resultQuery = new ArrayList<Book>();

		try {
			
			PreparedStatement statement = conn.prepareStatement("SELECT * FROM BOOKS"); // NOSONAR
			ResultSet resultSet = statement.executeQuery(); // NOSONAR

			while (resultSet.next()) {

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultQuery;
	}

	@Override
	public Book getBookByName(int isbn) {
		return null;
	}

	@Override
	public void saveBook(Book book) {
	}

	@Override
	public void deleteBook(Book book) {
	}
}

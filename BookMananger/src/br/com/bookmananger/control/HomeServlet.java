package br.com.bookmananger.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static br.com.bookmananger.constants.Constants.*;

/**
 * Servlet implementation class Home
 */
@WebServlet(PATH_SEPARATOR + HomeServlet.ENDPOINT)
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected static final String ENDPOINT = "home";
	protected static final String PAGE_NAME = "home";
    private static final String HOME_PAGE = BASE_PATH + PATH_SEPARATOR + PAGE_NAME + EXTENSION_JSP;   
	
    public HomeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rq = request.getRequestDispatcher(HOME_PAGE);
		rq.forward(request, response);	
	}
}

package br.com.bookmananger.control;

import static br.com.bookmananger.constants.Constants.BASE_PATH;
import static br.com.bookmananger.constants.Constants.EXTENSION_JSP;
import static br.com.bookmananger.constants.Constants.PATH_SEPARATOR;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bookmananger.dao.BookDaoImpl;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet(PATH_SEPARATOR + UserServlet.ENDPOINT)
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected static final String ENDPOINT = "user";
	protected static final String BASE_PAGES = PATH_SEPARATOR + "user" + PATH_SEPARATOR;
    private static final String USER_HOME_PAGE = BASE_PATH + BASE_PAGES + "user" + EXTENSION_JSP;
    private static BookDaoImpl bookDao;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        try {
			bookDao = new BookDaoImpl();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(USER_HOME_PAGE);
		bookDao.getAllBook();
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

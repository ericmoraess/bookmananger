package br.com.bookmananger.control;

import static br.com.bookmananger.constants.Constants.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bookmananger.dao.BookDaoImpl;
import br.com.bookmananger.model.Book;

/**
 * Servlet implementation class BooksServlet
 */
@WebServlet(PATH_SEPARATOR + BookServlet.ENDPOINT)
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected static final String ENDPOINT = "books";
	protected static final String BASE_PAGES = PATH_SEPARATOR + "book" + PATH_SEPARATOR;
	private static final String BOOK_HOME_PAGE = BASE_PATH + BASE_PAGES + "book" + EXTENSION_JSP;

	private BookDaoImpl bookDaoImpl;

	@Override
	public void init() throws ServletException {
		super.init();
		try {
			bookDaoImpl = new BookDaoImpl();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(BOOK_HOME_PAGE);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
}
